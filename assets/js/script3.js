/*********************************************************************************
   Module: WD004 JavaScript Programming
   Session 3: Control Structures 2

/*********************************************************************************
    LOOPS OR ITERATION
    Loops are control structures used to repeat a given section of code
    a certain number of times or until a particular condition is met.

    1. WHILE LOOP
    2. DO-WHILE LOOP
    3. FOR LOOP

    ============================================================================

    1. WHILE LOOP
    
        while (condition) {
            code aka "loop body"
            condition++; to prevent infinite loop
        }

        While the condition is true, the code from the loop body is executed.

        EXAMPLE: Count from 1 to 10
*/

    let num = 1;

    while (num <= 10) {
        console.log(num);
        num++;
    }


    //EXAMPLE: Display odd numbers FROM 1 to 10;

    console.log("Display odd numbers only from 1-10")
    num = 1;
    while (num <= 10) {
        if (num%2 !== 0) {
            console.log(num);
        }
        num++;
    }

    // or (below simpler approach)

    console.log("Display odd numbers only from 1-10 (simpler approach)")
    num = 1;
    while (num <= 10) {
        console.log(num)
        num+=2;
    }
    //1, 3, 5, 7, 9


    //EXAMPLE: Even numbers BETWEEN 1 to 10
    console.log("Even numbers BETWEEN 1 to 10")
    num = 1;
    while (num < 10) {
        if (num !== 1 && num !== 10) {
            if (num%2 !== 0) {
                console.log(num);
            }
        }
        num++;
    }

    // console.log("Even numbers BETWEEN 1 to 10")
    // num = 1;
    // while (num < 10) {
    //     if (num !== 1 && num !== 10) {
    //         if (num%2 !== 0) {
    //             console.log(num);
    //         }
    //     }
    //     num++;
    // }


/*
    2. DO-WHILE LOOP
        This loop will execute the code block once, before checking
        if the condition is true, then it will repeat the loop as long as
        the condition is true.

        do {
            loop body
            condition++; to prevent infinite loop
        } while (condition);
*/
    console.log("DO-WHILE LOOP")
    let num4 = 11;
    do {
        console.log(num4);
        num4++;
    } while (num4 < 10);


/*
    3. FOR LOOP
    for (initialize; condition; step of increment) {
        loop body
    }

    PARTS:
    initialize	            i = 0	    Executes once upon entering the loop.
    condition	            i < 3	    Checked before every loop iteration. If false, the loop stops.
    step of increment	    i++         Executes after the body on each iteration but before the condition check; 
                                        prevents infinite loop
    loop body	               	        Runs again and again while the condition is true.

    sequence: initialize > condition > body > increment
*/

// EXAMPLE: Count from 1 to 10
    console.log('FOR LOOP');
    for(let num5=1; num5<=10; num5++) {
        console.log(num5);
    }

// EXAMPLE: Iterate from 0 to 10 then compute the sum of all integers from 0 to 10.

    console.log("Iterate from 0 to 10 then compute the sum of all integers from 0 to 10")
    let sum = 0;
    for(let i=0; i<=10; i++) {
        sum = sum + i;      //sum += i;
        console.log(`sum has a new value of ${sum}`);
    }
