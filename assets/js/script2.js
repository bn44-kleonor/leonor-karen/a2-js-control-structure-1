// SWITCH CASE
/*
	SWITCH CASE
		let x = 1;

		switch(x) {
			case 'value1': // if (x==='value1')
				code to run if condition is true
				break;
			case 'value2': // if (x==='value2')
				code to run if condition is true
				break;
			default:
				code to run if condition does not fall to any case
				break;
		}
*/


let product = prompt("What do you want to buy?");		//prompt in the browser will appear
	console.log(product);

switch (product) {
	case 'chocolate':
	case 'cologne':
		alert (`${product} is P20.00`);
		break;
	case 'wine':
		alert (`${product} is P30.00`);
		break;
	case 'softdrinks':
	case 'shampoo':
	case 'conditioner':
		alert(`${product} is P40.00`);
		break;
	default:
		alert(`${product} is not available`);
}


