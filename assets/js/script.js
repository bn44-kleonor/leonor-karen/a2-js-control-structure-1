// Example expected result
// 0 is even
// 1 is odd
// two is not a number

// ans to activity - july 11
function showResult(num) {
	if(Number.isInteger(num)) {
		if(num%2 === 0) {
			console.log(`${num} is an even number`);
		} else {
			console.log(`${num} is an odd number`);
		}
	} else {
		if(typeof num === 'number') {
			console.log(`${num} is not an integer`);
		} else {
			console.log(`${num} is not a number`);
		}
	}
}

showResult(2.2);
showResult("one");
showResult(false);
showResult(true);
showResult(null);
showResult(undefined);
showResult(10);
showResult(9);
showResult(8);
showResult(7);
showResult(1);
showResult(0);