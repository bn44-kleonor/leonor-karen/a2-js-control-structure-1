// WD004-S3-A1
//Push as	a2-js-control-structure-1

/*================================================================================
MODULE: WD004-S3-A1  

Create a variable `day` and assign a day to it. Using switch-case:
If it's
Monday, output in the console "You need to wear Red",
Tuesday:  "You need to wear Orange",
Wednesday: "You need to wear Yellow",
Thursday: "You need to wear Green",
Friday: "You need to wear Blue",
Saturday: "You need to wear Indigo",
Sunday: "You need to wear Violet",
else "You should wear black".

Push as: a2-js-control-structures-1
==================================================================================*/



// let day = prompt("What day is today?")

// switch (day) {
// 	case 'Monday':
// 		alert("You need to wear Red");
// 		break;
// 	case 'Tuesday':
// 		alert("You need to wear Orange");
// 		break;
// 	case 'Wednesday':
// 		alert("You need to wear Yellow");
// 		break;
// 	case 'Thursday':
// 		alert("You need to wear Green");
// 		break;
// 	case 'Friday':
// 		alert("You need to wear Blue");
// 		break;
// 	case 'Saturday':
// 		alert("You need to wear Indigo");
// 		break;
// 	case 'Sunday':
// 		alert("You need to wear Violet");
// 		break;
// 	default:
// 		alert("You need to wear black");
// 		break;
// }



// streched goal (ignore case) e.g. monday, MONday, mONDay (etc)
// solution: make all the value of case be lower case. then, covert all the input of user to all lower case using "toLowerCase()"

let day = prompt("What day is today?")

switch (day.toLowerCase()) {
	case 'monday':
		alert("You need to wear Red");
		break;
	case 'tuesday':
		alert("You need to wear Orange");
		break;
	case 'wednesday':
		alert("You need to wear Yellow");
		break;
	case 'thursday':
		alert("You need to wear Green");
		break;
	case 'friday':
		alert("You need to wear Blue");
		break;
	case 'saturday':
		alert("You need to wear Indigo");
		break;
	case 'sunday':
		alert("You need to wear Violet");
		break;
	default:
		alert("You need to wear black");
		break;
}
